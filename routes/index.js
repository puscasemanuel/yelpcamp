const express = require('express');
const router = express.Router();
const passport = require('passport');
const User = require('../models/user');

//root
router.get('/', (req, res) => {
    res.render("landing");
});



//show register form
router.get('/register', (req, res) => {
    res.render('register'); 
})

//handle sign up logic
router.post('/register', (req, res) => {
    const newUser = new User({username: req.body.username});

    User.register(newUser, req.body.password, (err, user) => {
        if(err){
            req.flash("error", err.message);
            res.redirect('register');
        }

        passport.authenticate('local')(req, res, () => {
            req.flash("success", "Welcome to YelpCampp " + user.username);
            res.redirect('/campgrounds');
        })
    })
});


//Login routes
router.get('/login', (req, res) =>{
    res.render('login');
});

//handling login logic
router.post('/login', passport.authenticate("local", {successRedirect: "/campgrounds", failureRedirect: "/login"}), (req, res) =>{
    res.send('LOGIC HERE!');
});


//logout
router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success', 'Logged you out!');
    res.redirect('/campgrounds');
});

module.exports = router;