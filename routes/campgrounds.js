const express = require('express');
var router = express.Router();
const Campground = require('../models/campground');
const middleware = require('../middleware');

//Show all campgrounds
router.get('/', (req, res) => {
    Campground.find({}, (err, allcampgrounds)=> {
        if(err){
            console.log(err);
        }else {
            res.render("campgrounds/index", {campgrounds: allcampgrounds})
        }
    })
    //res.render("campgrounds", {campgrounds: campgrounds});
});

router.post("/", middleware.isLoggedIn, (req, res) => {
    //get data from form and add to campgrounds array
    const name = req.body.name;
    const image = req.body.image;
    const description = req.body.description;
    const price = req.body.price;
    const author = {
        id: req.user._id,
        username: req.user.username
    };
    Campground.create({
        name: name,
        image: image,
        description: description,
        price: price,
        author: author
    
    }, (err, data) => {
        if(err){
            console.log(err);
        }else {
            //redirect back to comeback campgrounds
            res.redirect("/campgrounds"); //default redirecs to get route
        }
    });
});

router.get("/new", middleware.isLoggedIn, (req, res) => {
    res.render("campgrounds/new");
});

router.get("/:id", (req, res) => {
    Campground.findById(req.params.id).populate("comments").exec((err, foundCampground) => {
        if(err || !foundCampground){
            req.flash("error", "Campground not found!");
            res.redirect('back');
        }else {
            res.render("campgrounds/show", {campground: foundCampground});
        }
    });
});

//EDIT CAMPGROUND
router.get("/:id/edit", middleware.checkCampgroundOwnership, (req, res) => {
        Campground.findById(req.params.id, (err, foundCampground) => {
            res.render("campgrounds/edit", {campground: foundCampground});
        });
});

//UPDATE
router.put("/:id", middleware.checkCampgroundOwnership, (req, res) => {
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err, updatedCampground) => {
        if(err){
            res.redirect("/campgrounds");
        }else {
            res.redirect('/campgrounds/' + req.params.id);
        }
    })
});


//DELETE
router.delete("/:id", middleware.checkCampgroundOwnership, (req, res) => {
    Campground.findByIdAndDelete(req.params.id, (err) => {
        if(err){
            res.redirect('/campgrounds');
            console.log(err);
        }else {
            res.redirect('/campgrounds');
        }
    })
});
module.exports = router;

